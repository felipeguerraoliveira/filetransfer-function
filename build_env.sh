#!/bin/bash
PWD=`pwd`

build_path="target"

if [ -d "$build_path" ]; then
    echo "Delete target folder..."
    rm -rf "./$build_path"
fi

activate_env() {
    echo "Activating virtual env..."
    source $build_path/bin/activate
}

if ! [ -d "$build_path" ]; then
    echo "Creating virtual env..."
    python3 -m venv "$build_path"
fi

activate_env

pip3 install -r requirements.txt --target ./target

cd target

zip -r ../build-package.zip .

cd ..

zip -g build-package.zip index.py