#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = "Felipe Guerra de Oliveira"

import os
import logging

import pysftp

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def create_sftp_connection(host_target, user, password, port):
    sftp_client = None

    try:
        cnopts = pysftp.CnOpts()
        cnopts.hostkeys = None

        sftp_client = pysftp.Connection(host_target, port=port, username=user, password=password, cnopts=cnopts)

        return sftp_client
    except Exception as ex:
        logger.error('Ocorreu um erro inesperado: %s : %s ' % (ex.__class__, ex))
        raise

def lambda_handler(event, context):
    logger.info('##### LAMBDA HANLDER BEGIN ######')

    return "OK"

if __name__ == "__main__":
    logger.info('##### Main Method ######')

    host = os.getenv('HOSTNAME_SFTP_SERVER');
    username = os.getenv('USERNAME_SFTP_SERVER');
    password = os.getenv('PASSWORD_SFTP_SERVER');
    home_dir = os.getenv('HOMEDIR_SFTP_SERVER');
    port = os.getenv('PORT_SFTP_SERVER')

    sftp_cli = create_sftp_connection(host, username, password, int(port))
    sftp_cli.chdir(home_dir)
    print(sftp_cli.listdir())